<?php

/**
 * @file
 * Enable of advanced navigation in terms.
 */

/**
 * Implementation of hook_menu().
 */
function taxotouch_menu() {
  $items = array();

  $items['admin/settings/taxotouch'] = array(
    'title' => 'Taxotouch',
    'description' => 'Advanced navigation in terms.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxotouch_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['taxotouch'] = array(
    'title' => t('Taxotouch'),
    'page callback' => 'taxotouch_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['taxotouch/%terms'] = array(
    'title' => t('Taxotouch'),
    'page callback' => 'taxotouch_page',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Get all terms from enabled vocabulary.
 *
 * @param $vocs
 *   Which vocabularys to generate array of terms.
 * @return
 *   An array of all terms enabled vocabulary.
 */
function taxotouch_get_terms($vocs = array()) {
  $terms = array();
  $show_empty = variable_get('taxotouch_show_empty_terms', FALSE);

  if (!empty($vocs)) {
    $query = 'SELECT t.tid, t.*, parent, COUNT(n.nid) node_count FROM {term_data} t INNER JOIN {term_hierarchy} h ON t.tid = h.tid '. ($show_empty ? 'LEFT' : 'INNER') .' JOIN {term_node} n ON t.tid = n.tid WHERE t.vid IN ('. db_placeholders($vocs) .') GROUP BY t.tid ORDER BY name';

    $result = db_query(db_rewrite_sql($query, 't', 'tid'), $vocs);
    while ($term = db_fetch_object($result)) {
      $terms[] = $term;
    }
  }

  return $terms;
}

function taxotouch_get_attendant_terms($arg_terms) {
  $query = 'SELECT DISTINCT(tn.tid), td.name FROM {term_node} tn INNER JOIN {term_data} td ON tn.tid = td.tid WHERE tn.nid IN ( SELECT nid FROM {term_node} WHERE tid = %d)';
  foreach ($arg_terms as $key => $tid) {
    if ($key == 0) continue;
    $query .= ' AND tn.nid IN (SELECT nid FROM {term_node} WHERE tid = %d)';
  }

  $result = db_query(db_rewrite_sql($query, 'tn', 'tid'), $arg_terms);

  $attendant_terms = array();
  while ($term = db_fetch_object($result)) {
    $attendant_terms[] = $term->tid;
  }

  return $attendant_terms;
}

/**
 * Get title for taxotouch page
 * 
 * @param $tids
 *    Array of tid selected terms
 * @return
 *   Title page
 */
function taxotouch_get_title($tids) {
  $terms = db_query('SELECT * FROM {term_data} WHERE tid IN ('. db_placeholders($tids) .')', $tids);

  $title = array();
  while ($term = db_fetch_object($terms)) {
    $title[] = $term->name;
  }

  return implode(', ', $title);
}

/**
 * Implementation of hook_block().
 */
function taxotouch_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks[0] = array(
      'info' => t('Taxotouch'),
    );

    return $blocks;
  }
  else if ($op == 'view') {
    $enable_vocs = variable_get('taxotouch_enable_vocs', array(0));
    $taxonomy_terms = taxotouch_get_terms($enable_vocs);

    $output = theme('taxotouch_term_list', array(), array(), $taxonomy_terms);

    $block = array(
      'subject' => t('Taxotouch'),
      'content' => $output,
    );
    return $block;
  }
}

/**
 * Menu callback; displays all terms in vocabulary and nodes associated with a terms.
 */
function taxotouch_page($terms = '') {
  $output = '';
  $enable_vocs = variable_get('taxotouch_enable_vocs', array(0));
  $show_term_list = variable_get('taxotouch_show_term_list', TRUE);

  $arg_terms = explode(',', $terms);

  if ($show_term_list) {
    $attendant_terms = taxotouch_get_attendant_terms($arg_terms);
    $taxonomy_terms = taxotouch_get_terms($enable_vocs);
    
    $output .= theme('taxotouch_term_list', $arg_terms, $attendant_terms, $taxonomy_terms);
  }

  if (!empty($terms)) {
    $select_nodes = taxonomy_select_nodes($arg_terms, 'and', 0, TRUE);

    $output .= taxonomy_render_nodes($select_nodes);
  }

  drupal_set_title(taxotouch_get_title($arg_terms));

  return $output;
}

/**
 *
 * @param max_nodes
 *   return max count nodes
 * @param $min_nodes
 *   return min count nodes
 * @param $taxonomy_terms
 */
function taxotouch_get_maxmin_nodes(&$max_nodes, &$min_nodes, $taxonomy_terms) {
  if (empty($taxonomy_terms)) {
    $max_nodes = 1;
    $min_nodes = 1;
  }

  $max_nodes = ($taxonomy_terms[0]->node_count == 0 ? 1 : $taxonomy_terms[0]->node_count);
  $min_nodes = $max_nodes;
  foreach ($taxonomy_terms as $term) {
    if ($max_nodes < $term->node_count) {
      $max_nodes = $term->node_count;
    }
    else if ($min_nodes > $term->node_count && $term->node_count != 0) {
      $min_nodes = $term->node_count;
    }
  }
}

/**
 * Implementation of hook_init().
 */
function taxotouch_init() {
  drupal_add_css(drupal_get_path('module', 'taxotouch') .'/taxotouch.css');
}

/**
 * Implementation of hook_theme()
 */
function taxotouch_theme() {
  return array(
    'taxotouch_term_link' => array(
      'arguments' => array('term' => NULL, 'arg_terms' => NULL, 'attendant_terms' => NULL, 'max_nodes' => 0, 'min_nodes' => 0),
    ),
    'taxotouch_term_list' => array(
      'arguments' => array('terms' => NULL, 'tree' => NULL, 'term_on' => NULL),
    ),
  );
}

/**
 * Render of term link.
 *
 * @param $term
 *   An object of term.
 * @return themeable
 */
function theme_taxotouch_term_link($term, $arg_terms, $attendant_terms, $max_nodes = 1, $min_nodes = 1) {
  $show_count_nodes = variable_get('taxotouch_show_count_nodes', FALSE);
  $empty_terms_as_text = variable_get('taxotouch_empty_terms_as_text', FALSE);
  $cloud_style = variable_get('taxotouch_cloud_style', FALSE);

  if (in_array($term->tid, $arg_terms)) {
    $link_term = array_diff($arg_terms, array($term->tid));
    $class = 'selected';
    $url = url('taxotouch/') . implode(',', $link_term);
    $title = $term->name;
  }
  else if (in_array($term->tid, $attendant_terms)) {
    $class = 'attendant';
    $url = url('taxotouch/') . implode(',', $arg_terms) .','. $term->tid;
    $title = $term->name;
  }
  else {
    $class = 'free';
    $url = url('taxotouch/') . $term->tid;
    $title = $term->name;
  }

  if ($show_count_nodes) {
    $title .= ' ('. $term->node_count .')';
  }

  if ($term->node_count == 0) {
    $class .= ' empty';
  }

  if ($cloud_style && $term->node_count) {
    $cloud = 1;
    if ($max_nodes != $min_nodes) {
      $cloud = ceil(($term->node_count - $min_nodes) / (($max_nodes - $min_nodes) / 5));
    }
    $class .= ' cloud-'. ($cloud != 0 ? $cloud : 1);
  }

  $output = '<a class="'. $class .'" href="'. $url .'">'. $title .'</a> ';

  if ($empty_terms_as_text && $term->node_count == 0) {
    $output = '<span class="empty">'. $title .'</span> ';
  }

  return $output;
}

/**
 * Render of terms list.
 *
 * @param $arg_terms
 *   An array of term ids from arguments.
 * @param $attendant_terms
 *   An array of attendant term ids
 * @param $taxonomy_terms
 *   An array of all terms in vocabulary
 *
 * @return themeable
 */
function theme_taxotouch_term_list($arg_terms, $attendant_terms, $taxonomy_terms) {
  $show_capital_letters = variable_get('taxotouch_show_capital_letters', FALSE);
  $cloud_style = variable_get('taxotouch_cloud_style', FALSE);

  if ($cloud_style) {
    taxotouch_get_maxmin_nodes($max_nodes, $min_nodes, $taxonomy_terms);
  }

  $output = '<div class="taxotouch-term-list">';

  $letter = '';
  foreach ($taxonomy_terms as $term) {
    $letter_new = drupal_strtoupper(drupal_substr($term->name, 0, 1));

    if ($letter != $letter_new && $show_capital_letters) {
      $output .= ' <span class="capitally">'. $letter_new .'</span>';
      $letter = $letter_new;
    }

    $output .= theme('taxotouch_term_link', $term, $arg_terms, $attendant_terms, $max_nodes, $min_nodes);
  }

  if (empty($taxonomy_terms)) {
    $output .= t('Please, enable vocabularys in <a href="@settings">settings</a> or add terms.', array('@settings' => url('admin/settings/taxotouch')));
  }

  $output .= '</div><div style="clear:both;"></div>';
  return $output;
}

/**
 * Menu callback for the taxotouch settings form.
 */
function taxotouch_admin_settings() {

  $form['taxotouch_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
  );
  $form['taxotouch_display']['taxotouch_show_count_nodes'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Show count of nodes'),
    '#default_value' => variable_get('taxotouch_show_count_nodes', FALSE),
    '#description' => t('Show count of nodes for every term.'),
  );
  $form['taxotouch_display']['taxotouch_show_empty_terms'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Show empty terms'),
    '#default_value' => variable_get('taxotouch_show_empty_terms', FALSE),
    '#description' => t('Show terms without nodes.'),
  );
  $form['taxotouch_display']['taxotouch_empty_terms_as_text'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Show empty terms as simple text'),
    '#default_value' => variable_get('taxotouch_empty_terms_as_text', FALSE),
    '#description' => t('If you checked "Show empty text" this options replace link on a simple text.'),
  );
  $form['taxotouch_display']['taxotouch_show_capital_letters'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Show capital letters'),
    '#default_value' => variable_get('taxotouch_show_capital_letters', TRUE),
    '#description' => t('Show capital letters in list of terms.'),
  );
  $form['taxotouch_display']['taxotouch_cloud_style'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Use tag cloud style'),
    '#default_value' => variable_get('taxotouch_cloud_style', FALSE),
    '#description' => t('Show list terms as tag cloud.'),
  );
  $form['taxotouch_display']['taxotouch_show_term_list'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Show term list'),
    '#default_value' => variable_get('taxotouch_show_term_list', TRUE),
    '#description' => t('Show term list on page taxotouch.'),
  );
  $form['taxotouch_vocabulary'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings of vocabulary'),
  );

  $options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name;
  }
  if (!empty($vocabularies)) {
    $form['taxotouch_vocabulary']['taxotouch_enable_vocs'] =  array(
      '#type' => 'checkboxes',
      '#title' => t('Enable vocabulary'),
      '#options' => $options,
      '#default_value' => variable_get('taxotouch_enable_vocs', array(0)),
    );
  }
  else {
    $form['taxotouch_vocabulary']['taxotouch_no_one'] = array(
      '#type' => 'markup',
      '#value' => t('Vocabulary not found.'),
    );
  }

  return system_settings_form($form);
}